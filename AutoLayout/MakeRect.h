//
//  MainRect.h
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>

#define viewCenter_x [UIScreen mainScreen].bounds.size.width/2 - 100
#define viewCenter_y [UIScreen mainScreen].bounds.size.height/2 - 100

@interface MakeRect : UIView

@end
