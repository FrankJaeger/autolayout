//
//  MainRect.m
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "MakeRect.h"

@implementation MakeRect

- (id)initWithFrame:(CGRect)frame {
    self = [ super initWithFrame:frame ];
    
    self.frame = frame;
    self.backgroundColor = [ UIColor colorWithRed:253/255.f green:196/255.f blue:64/255.f alpha:1 ];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
