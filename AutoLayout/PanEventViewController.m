//
//  PanEventViewController.m
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "PanEventViewController.h"


@interface PanEventViewController ()

@property CGFloat yLimit;
@property NSInteger taskLabelAlpha;

@end

@implementation PanEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mainRect = [ [MakeRect alloc] initWithFrame:CGRectMake(viewCenter_x, viewCenter_y, 200, 200) ];
    
    [ self.view addSubview:_mainRect ];
    
    _yLimit = _mainRect.frame.origin.y;
    
    UIPanGestureRecognizer *panGRec = [[ UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestRecognizer:) ];
    
    [ _mainRect addGestureRecognizer:panGRec ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Pan gesture recognizer

- (void)panGestRecognizer:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [ recognizer translationInView:self.view ];
    
    if ( recognizer.view.frame.origin.y + translation.y < _yLimit  ) {
        [UIView animateWithDuration:0.2 animations:^{
            recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                                 recognizer.view.center.y + translation.y
                                                 );
        }];
    }
    [ recognizer setTranslation:CGPointMake(0, 0) inView:self.view ];
    
    _taskLabelAlpha = ( recognizer.view.frame.origin.y <= ( _taskLabel.frame.origin.y + _taskLabel.frame.size.height ) ) ? 0 : 1;
    
    [ UIView animateWithDuration:0.5 animations:^{
        _taskLabel.alpha = _taskLabelAlpha;
    }];
    
    if ( recognizer.view.frame.origin.y <= 0 ) {
        [ UIView animateWithDuration:0.5 animations:^{
            _doneButton.alpha = 1;
        }];
    }
}


@end
