//
//  PinchEventViewController.m
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "PinchEventViewController.h"

@interface PinchEventViewController ()

@property NSInteger taskLabelAlpha;

@end

@implementation PinchEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIPinchGestureRecognizer *pinchRec = [[ UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestRec:) ];

    [ _mainRect addGestureRecognizer:pinchRec ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark Pinch recognizer

- (void)pinchGestRec:(UIPinchGestureRecognizer *)recognizer {
    [ UIView animateWithDuration:0.2 animations:^{
        recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
        recognizer.scale = 1;
    }];
    
    _taskLabelAlpha = ( recognizer.view.frame.origin.y <= ( _taskLabel.frame.origin.y + _taskLabel.frame.size.height ) ) ? 0 : 1;
    
    [ UIView animateWithDuration:1 animations:^{
        _taskLabel.alpha = _taskLabelAlpha;
    }];
    
    if ( (recognizer.view.frame.size.height >= [UIScreen mainScreen].bounds.size.height) && (recognizer.view.frame.size.width  >= [UIScreen mainScreen].bounds.size.width) ) {
            [ UIView animateWithDuration:0.5 animations:^{
                [ _doneButton setAlpha:1 ];
            }];
    }
    
}

@end
