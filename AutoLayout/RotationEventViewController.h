//
//  RotationEventViewController.h
//  AutoLayout
//
//  Created by Przemek on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeRect.h"

@interface RotationEventViewController : UIViewController

@property (strong, nonatomic) MakeRect *mainRect;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *taskLabel;
@property (strong, nonatomic) UIImageView *sunGlasses;

@end
