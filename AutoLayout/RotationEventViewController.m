//
//  RotationEventViewController.m
//  AutoLayout
//
//  Created by Przemek on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "RotationEventViewController.h"
#import <PureLayout/PureLayout.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface RotationEventViewController ()

@property (strong, nonatomic) UIRotationGestureRecognizer *rotGRec;
@property BOOL animateRollin;
@property AVAudioPlayer *aPlayer;

@property (strong, nonatomic) NSLayoutConstraint *sunGlassesConstr;

@end

@implementation RotationEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _mainRect = [ MakeRect newAutoLayoutView ];
    _sunGlasses = [ UIImageView newAutoLayoutView ];
    
    
    [ self.view addSubview:_mainRect ];
    [ self.view addSubview:_sunGlasses ];
    
    _sunGlasses.image = [ UIImage imageNamed:@"sunG" ];
    
    _rotGRec = [ [ UIRotationGestureRecognizer alloc ] initWithTarget:self action:@selector(theySeeMeRollin:) ];
    [ _mainRect addGestureRecognizer:_rotGRec ];
    
    _animateRollin = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateViewConstraints {
    
    [ _mainRect autoCenterInSuperview ];
    [ _mainRect autoSetDimensionsToSize:CGSizeMake(200, 200) ];
    
    [ _sunGlasses autoSetDimensionsToSize:CGSizeMake(170, 170) ];
    [ _sunGlasses autoAlignAxisToSuperviewAxis:ALAxisVertical ];
    _sunGlassesConstr = [ _sunGlasses autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:10 ];
    [ _sunGlasses setAlpha:0 ];
    
    [ super updateViewConstraints ];
}

- (void)theySeeMeRollin:(UIRotationGestureRecognizer *)recognizer {
    [ UIView animateWithDuration:0.2 animations:^{
        _mainRect.transform = CGAffineTransformMakeRotation( recognizer.rotation );
    }];
    
    if ( atan2f(_mainRect.transform.b, _mainRect.transform.a) > 2.5 ) {
        [ _mainRect removeGestureRecognizer:_rotGRec ];
        
        [ UIView animateWithDuration:0.5 animations:^{
            _doneButton.alpha = 1;
            _taskLabel.alpha = 0;
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [ self startRollin ];
            [ self.view layoutIfNeeded ];
            
            [ UIView animateWithDuration:3 animations:^{
                _sunGlassesConstr.constant = [ UIScreen mainScreen ].bounds.size.height/2 - 100;
                _sunGlasses.alpha = 1;
                [ self.view layoutIfNeeded ];
            }];
            
            NSString *patch = [ [NSBundle mainBundle] pathForResource:@"tsmr" ofType:@"mp3" ];
            NSError *error = nil;
            
            NSURL *url = [ NSURL fileURLWithPath:patch ];
            self.aPlayer = [ [AVAudioPlayer alloc] initWithContentsOfURL:url error:&error ];
            [ self.aPlayer play ];
            [ self.aPlayer setNumberOfLoops:-1 ];
        });
    }
    
    switch ( recognizer.state ) {
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateFailed:
            if ( atan2f(_mainRect.transform.b, _mainRect.transform.a) < 2.5 ) {
                [ UIView animateWithDuration:0.2 animations:^{
                    _mainRect.transform = CGAffineTransformMakeRotation( 0 );
                }];
            }
    }
}

- (void)startRollin {
    [ UIView animateWithDuration:0.5 delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        _mainRect.transform = CGAffineTransformRotate(_mainRect.transform, M_PI/2 );
    } completion:^(BOOL finished) {
        if (finished) {
            if (_animateRollin) {
                [self startRollin];
            }
        }
    }];

}

- (IBAction)doneButtonClick:(id)sender {
    exit(0);
}


@end
