//
//  SwipeEventViewController.h
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeRect.h"

#define mainRect_wh_size 200
#define rectFriends_offset 30

enum {
    sPLeft = -(mainRect_wh_size + rectFriends_offset),
    sPCenter = 0,
    sPRight = (mainRect_wh_size + rectFriends_offset)
} swipePositions;

@interface SwipeEventViewController : UIViewController

@property (strong, nonatomic) MakeRect *mainRect;
@property (strong, nonatomic) MakeRect *mainRectRFriend;
@property (strong, nonatomic) MakeRect *mainRectLFriend;
@property (strong, nonatomic) UIView *rectFriendsMask;

@property (strong, nonatomic) IBOutlet UIView *leftLED;
@property (strong, nonatomic) IBOutlet UIView *rightLED;

@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@end
