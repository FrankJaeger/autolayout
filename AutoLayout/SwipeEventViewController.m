//
//  SwipeEventViewController.m
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "SwipeEventViewController.h"
#import <PureLayout/PureLayout.h>

@interface SwipeEventViewController ()

@property (assign, nonatomic) BOOL didSetupConstraints;
@property (strong, nonatomic) NSMutableArray *friendsConstraints;
@property (strong, nonatomic) NSLayoutConstraint *swipeConstraint;

@end

@implementation SwipeEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _friendsConstraints = [ NSMutableArray new ];
    
    NSArray *rectFriends = @[ @"_mainRectLFriend", @"_mainRect", @"_mainRectRFriend", @"_rectFriendsMask" ];
    
    for ( NSString *rectName in rectFriends ) {
        [ self setValue:[ MakeRect newAutoLayoutView ] forKey:rectName ];
        
        [ self.view addSubview:[ self valueForKey:rectName ] ];
    }
    
    UISwipeGestureRecognizer *swipeGRecRight = [ [ UISwipeGestureRecognizer alloc ] initWithTarget:self action:@selector(swipeGestRecognizerRight:) ];
    UISwipeGestureRecognizer *swipeGRecLeft = [ [ UISwipeGestureRecognizer alloc ] initWithTarget:self action:@selector(swipeGestRecognizerLeft:) ];
    
    swipeGRecLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeGRecRight.direction = UISwipeGestureRecognizerDirectionRight;
    
    [ _rectFriendsMask addGestureRecognizer:swipeGRecRight ];
    [ _rectFriendsMask addGestureRecognizer:swipeGRecLeft ];
    
    _rectFriendsMask.backgroundColor = [ UIColor colorWithRed:0 green:0 blue:0 alpha:0 ];
    
}

- (void)updateViewConstraints {
    
    if ( !_didSetupConstraints ) {
    
        [ _rectFriendsMask autoSetDimension:ALDimensionHeight toSize:mainRect_wh_size ];
        [ _rectFriendsMask autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0 ];
        [ _rectFriendsMask autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0 ];
    
        [ _rectFriendsMask autoCenterInSuperview ];
        
        [ _mainRectLFriend autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:_mainRect withOffset:-rectFriends_offset ];
        [ _mainRectRFriend autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_mainRect withOffset:rectFriends_offset ];
    
        [ _mainRectLFriend autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_mainRect ];
        [ _mainRectRFriend autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_mainRect ];
    
        [ _mainRect autoSetDimensionsToSize:CGSizeMake(mainRect_wh_size, mainRect_wh_size)];
        [ _mainRectLFriend autoSetDimensionsToSize:CGSizeMake(mainRect_wh_size, mainRect_wh_size)];
        [ _mainRectRFriend autoSetDimensionsToSize:CGSizeMake(mainRect_wh_size, mainRect_wh_size)];
        
        [ _mainRect autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.view ];
        
        _swipeConstraint = [ _mainRect autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view withOffset:0 ];
    
        _didSetupConstraints = YES;
    }
    
    
    [ super updateViewConstraints ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Swipe Gesture Recognizer

- (void)swipeGestRecognizerLeft:(UISwipeGestureRecognizer *)recognizer {
    [ self.view layoutIfNeeded ];
    
    if ( _swipeConstraint.constant != sPLeft ){
        [ UIView animateWithDuration:0.2
                        delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                        animations:^{
                            _swipeConstraint.constant -= ( mainRect_wh_size + rectFriends_offset );
                            if ( _swipeConstraint.constant == sPLeft ) {
                                _rightLED.backgroundColor = [UIColor colorWithRed:0.40 green:0.84 blue:0.39 alpha:1.0];
                            }
                            [ self.view layoutIfNeeded ];
                        }
                        completion:^(BOOL finished) {
                          
                        }];
    }
    
    [ self checkLEDsState ];
}

- (void)swipeGestRecognizerRight:(UISwipeGestureRecognizer *)regognizer {
    [ self.view layoutIfNeeded ];
    
    if ( _swipeConstraint.constant != sPRight ) {
        [ UIView animateWithDuration:0.2
                           delay:0.0
                         options:UIViewAnimationOptionCurveEaseInOut
                      animations:^{
                          _swipeConstraint.constant += ( mainRect_wh_size + rectFriends_offset );
                          if ( _swipeConstraint.constant == sPRight ) {
                            _leftLED.backgroundColor = [UIColor colorWithRed:0.40 green:0.84 blue:0.39 alpha:1.0];
                          }
                          [ self.view layoutIfNeeded ];
                      }
                      completion:^(BOOL finished) {
                          
                      }];
    }
    
    [ self checkLEDsState ];
}

- (void)checkLEDsState {
    if ( [ _leftLED.backgroundColor isEqual:_rightLED.backgroundColor ] ) {
        [ UIView animateWithDuration:0.5 animations:^{
            _doneButton.alpha = 1;
        }];
    }
}


@end
