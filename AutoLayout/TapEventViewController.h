//
//  TapEventViewController.h
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TapEventViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIView *mainRect;
@property (weak, nonatomic) IBOutlet UILabel *counterLabel;

@end
