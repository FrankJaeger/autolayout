//
//  TapEventViewController.m
//  AutoLayout
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "TapEventViewController.h"

@interface TapEventViewController ()

@property NSInteger tapCounter;

@end

@implementation TapEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tapCounter = 0;
    
    UITapGestureRecognizer *tapGRec = [[ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(tapGestResponse:) ];
    
    [ _mainRect addGestureRecognizer:tapGRec ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   }

#pragma mark Gesture response

- (void)tapGestResponse:(UITapGestureRecognizer *)recognizer {
    _counterLabel.text = [NSString stringWithFormat:@"Count = %ld", ++_tapCounter];
    
    if ( _tapCounter >= 5 ) {
        [UIView animateWithDuration:0.5 animations:^{
            [ _doneButton setAlpha:1 ];
        }];
    }
    
    }

#pragma mark Tap occur



@end
